import { Response, Request, NextFunction, Router } from "express"

import createError from "http-errors"
import express from "express"
import path from "path"
import cookieParser from "cookie-parser"
import logger from "morgan";
import UserRouter from "./routes/User"
import authRouter from "./routes/auth"
import serverRouter from "./routes/S3"

const cors = require("cors");
const helmet = require("helmet");
const app = express();


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(cors({
  origin: ["http://localhost:3000", "https://laurent_techer-dev.surge.sh/"],
  credentials: true
}))
app.use(helmet());
app.use(express.json());

// app.use((req, res, next) => {
//   res.setHeader('Access-Control-Allow-Origin', '*');
//   res.setHeader(
//     'Access-Control-Allow-Headers',
//     'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization'
//   );
//   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
//   next();
// });

app.use("/user", UserRouter)
app.use("/auth", authRouter);
app.use("/server", serverRouter)


app.get('/data', (req: any, res: any) => {
  const data = 'Hello from the backend';

  res.status(200).json({ data: data });
})






//catch 404 and forward to error handler
// app.use(function (req, res, next) {
//   next(createError(404));
// });
// // error handler
// app.use(function (err: any, req: Request, res: Response, next: NextFunction) {
//   // set locals, only providing error in development
//   console.log(err.message)
//   res.locals.message = err.message;

//   // render the error page
//   res.status(err.status || 500);
//   res.json({ message: err.message })
// });




export default app
