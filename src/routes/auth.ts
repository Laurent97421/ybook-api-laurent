import express from "express"
import validateForm from "../controllers/User/handlers/validateForm";
import { PrismaClient } from '@prisma/client'
const bcrypt = require("bcrypt");
const prisma = new PrismaClient()
const authRouter = express.Router();


authRouter.post("/", (req, res) => {
    res.send({response: "ok"})
});


authRouter.post("/login", (req, res) => {
    validateForm(req, res);
});

authRouter.post("/register", async (req, res) => {
    validateForm(req, res);


    const existingUser = await prisma.user.findUnique({
        where: {
            email: req.body.email
        }
    })

    if (existingUser == null) {
        //register
        const firstname = req.body.firstname;
        const lastname = req.body.lastname;
        const email = req.body.email;
        const newUserQuery = await prisma.user.create({
            data: {
                firstname,
                lastname,
                email,

            }
        });
        res.json({ loggedIn: true, email });
    } else {
        res.json({ loggedIn: false, status: "Email déjà utilisé" })
    }


});

export default authRouter;