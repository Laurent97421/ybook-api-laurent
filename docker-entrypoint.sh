#!/bin/bash

set -e

if [ "$NODE_ENV" = "development" ]; then
  npx prisma generate
fi

exec "$@"